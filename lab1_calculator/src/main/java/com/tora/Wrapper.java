package com.tora;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

/**
 * Hello world!
 *
 */
public class Wrapper
{
    public static void main( String[] args ) {

        Calculator calc = new Calculator();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String cmd;

        System.out.println("|---------------------------------------------|");
        System.out.println("|--------------- Possible Commands: ----------|");
        System.out.println("|---------------    X OPERATOR Y    ----------|");
        System.out.println("|---------------       sqrt X       ----------|");
        System.out.println("|---------------        last        ----------|");
        System.out.println("|---------------        EXIT        ----------|");
        System.out.println("|---------------     Operators:     ----------|");
        System.out.println("|---------------  '+' '-' '/' '*'   ----------|");
        System.out.println("|---------------    'min' 'max'     ----------|");
        System.out.println("|---------------------------------------------|");

        while (true) {
            try {
                System.out.println("Enter a command -> ");

                cmd = br.readLine();

                if(Objects.equals(cmd, "EXIT"))
                    break;

                System.out.println(calc.parse_command(cmd));
            } catch (IOException | RuntimeException e) {
                e.printStackTrace();
            }
        }
    }
}
