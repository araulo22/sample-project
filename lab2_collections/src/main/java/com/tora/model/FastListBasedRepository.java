package com.tora.model;


import org.eclipse.collections.impl.list.mutable.FastList;

public class FastListBasedRepository<T> implements InMemoryRepository<T> {
    private FastList<T> list = new FastList<>();

    public FastListBasedRepository() {}

    @Override
    public void add(T item) {
        list.add(item);
    }

    @Override
    public boolean contains(T item) {
        return list.contains(item);
    }

    @Override
    public void remove(T item) {
        list.remove(item);
    }
}
