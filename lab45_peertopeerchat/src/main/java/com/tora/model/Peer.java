package com.tora.model;

import javax.json.Json;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.Socket;
import java.util.*;

public class Peer {
    static Server server;
    static String username, port;
    private Map<String, PeerThread> peerThreadMap = new HashMap<>();
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("> Enter username for this peer: ");
        username = bufferedReader.readLine();
        System.out.println("> Enter port for this peer: ");
        port = bufferedReader.readLine();
        server = new Server(port, username);
        server.start();
        new Peer().startup(bufferedReader);
    }
    public void startup(BufferedReader bufferedReader) throws Exception {
        System.out.println("> List of commands:\n> !hello <port> - open connection\n> !bye <port> - close connection\n> !byebye - close all connections and exit");
        chat(bufferedReader);
    }
    public void chat(BufferedReader bufferedReader) {
        try {
            while (true) {
                String message = bufferedReader.readLine();
                String command = parseCommand(message);
                if(command.equals("byebye")) {
                    break;
                } else if (command.equals("null")) {
                    createSendMessage(server, username, message);
                }
            }
            System.exit(0);
        } catch (Exception e) {

        }
    }
    //Returns:
    //String - command name OR null for command not recognized
    private String parseCommand(String command) {
        String[] params = command.split(" ");
        if(params[0].equals("!hello")) {
            try {
                initializeSocket(params[1]);
            } catch (Exception e) {
                System.out.println("Could not initialize connection, check input.");
            }
            return "hello";
        } else if (params[0].equals("!bye")) {
            return "bye";
        } else if (params[0].equals("!byebye")) {
            return "byebye";
        } else
            return "null";
    }
    private void initializeSocket(String port) throws IOException {
        Socket socket = null;
        try {
            if(!peerThreadMap.containsKey(port)) {
                System.out.println("> Initializing connection to port " + port + "...");
                socket = new Socket("localhost", Integer.parseInt(port));
                PeerThread peerThread = new PeerThread(socket, this);
                peerThreadMap.put(port, peerThread);
                peerThread.start();
            } else {
                System.out.println("Connection already established to port " + port + ".");
            }
        } catch (Exception e) {
            if(socket != null) socket.close();
            else System.out.println("Could not initialize connection, check input.");
        }
    }

    private void createSendMessage(Server server, String username, String message) {
        StringWriter stringWriter = new StringWriter();
        Json.createWriter(stringWriter).writeObject(Json.createObjectBuilder()
                .add("username", username)
                .add("message", message)
                .build());
        server.sendMessage(stringWriter.toString());
    }

    public Map<String, PeerThread> getPeerThreadMap() {
        return peerThreadMap;
    }
}
