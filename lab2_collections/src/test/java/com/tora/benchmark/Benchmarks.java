package com.tora.benchmark;

import com.tora.model.*;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 1, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 2, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@State(Scope.Benchmark)
public class Benchmarks {
    @Param({"1", "31", "65", "101", "103", "1024", "10240", "65535", "21474836"})
    public int size;

    ArrayListBasedRepository<Integer> al_repo = new ArrayListBasedRepository<>();
    FastListBasedRepository<Integer> fl_repo = new FastListBasedRepository<>();
    HashSetBasedRepository<Integer> hs_repo = new HashSetBasedRepository<>();
    TreeSetBasedRepository<Integer> ts_repo = new TreeSetBasedRepository<>();

    @Benchmark
    public void test_array_list_based() {
        for(int i = 0; i < size; i++) {
            al_repo.add(i);
        }
    }

    @Benchmark
    public void test_fast_list_based() {
        for(int i = 0; i < size; i++) {
            fl_repo.add(i);
        }
    }

    @Benchmark
    public void test_hash_set_based() {
        for(int i = 0; i < size; i++) {
            hs_repo.add(i);
        }
    }

    @Benchmark
    public void test_tree_set_based() {
        for(int i = 0; i < size; i++) {
            ts_repo.add(i);
        }
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(Benchmarks.class.getSimpleName())
                .forks(1)
                .build();

        new Runner(opt).run();
    }

}
