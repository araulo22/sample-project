package com.tora.model;

import java.util.ArrayList;

public class ArrayListBasedRepository<T> implements InMemoryRepository<T> {
    private ArrayList<T> list = new ArrayList<>();

    public ArrayListBasedRepository() {}

    @Override
    public void add(T item) {
        list.add(item);
    }

    @Override
    public boolean contains(T item) {
        return list.contains(item);
    }

    @Override
    public void remove(T item) {
        list.remove(item);
    }
}
