package com.tora.model;

import javax.json.Json;
import javax.json.JsonObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class PeerThread extends Thread {
    private BufferedReader bufferedReader;
    private Socket socket;
    private Peer peer;
    private boolean sent_ack = false;
    public PeerThread(Socket socket, Peer peer) throws IOException {
        this.socket = socket;
        this.peer = peer;
        bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }
    public void run() {
        boolean running = true;
        while (running) {
            try {
                JsonObject jsonObject = Json.createReader(bufferedReader).readObject();
                if(jsonObject.containsKey("username")) {
                    //Check for !ack
                    if(sent_ack) {
                        if(!jsonObject.getString("message").equals("!ack"))
                            System.out.println("[" + jsonObject.getString("username") + "]: " + jsonObject.getString("message"));
                    } else if(jsonObject.getString("message").equals("!ack")) {
                        sent_ack = true;
                        System.out.println("[" + jsonObject.getString("username") + "]: " + jsonObject.getString("message"));
                    } else {
                        System.out.println(socket.getPort() + " did not acknowledge the connection.");
                        peer.getPeerThreadMap().remove(String.valueOf(socket.getPort()));
                        running = false;
                        interrupt();
                    }
                }
            } catch (Exception e) {
                System.out.println(socket.getPort() + " has disconnected.");
                peer.getPeerThreadMap().remove(String.valueOf(socket.getPort()));
                running = false;
                interrupt();
            }
        }
    }
}
