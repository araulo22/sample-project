package com.tora.model;

import java.util.HashSet;

public class HashSetBasedRepository<T> implements InMemoryRepository<T> {
    private HashSet<T> set = new HashSet<T>();;

    public HashSetBasedRepository() {}

    @Override
    public void add(T item) {
        set.add(item);
    }

    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }

    @Override
    public void remove(T item) {
        set.remove(item);
    }
}
