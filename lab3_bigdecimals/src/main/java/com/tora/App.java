package com.tora;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collector;
import java.util.Comparator;

import com.google.common.collect.Comparators;
import com.google.common.collect.Lists;


public class App 
{
    //CONSTANTS
    private static final int LIST_SIZE = 10;
    private static final String MAX_RANDOM = "2";
    private static final int TOP_PERCENT = 20;
    private static BigDecimal randBigDecimal(String range) {
        BigDecimal max = new BigDecimal(range);
        BigDecimal rand = new BigDecimal(Math.random()).multiply(max);
        return rand;
    }

    private static ArrayList<BigDecimal> generateList(int size) {
        ArrayList<BigDecimal> list = new ArrayList<>();
        for(int i = 0; i < size; i++) {
            list.add(randBigDecimal(MAX_RANDOM));
        }
        return list;
    }

    private static BigDecimal add(ArrayList<BigDecimal> list) {
        BigDecimal result = list.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
        return result;
    }

    private static BigDecimal average(ArrayList<BigDecimal> list) {
        BigDecimal result = add(list);
        return result.divide(new BigDecimal(list.size()), BigDecimal.ROUND_DOWN);
    }

    private static List<BigDecimal> topPercent(int percent, List<BigDecimal> list) {
        double p = percent / 100.0;
        p *= list.size();
        int k_numbers = (int) Math.round(p);
        final Collector<BigDecimal, ?, List<BigDecimal>> collector = Comparators.greatest(k_numbers, Comparator.<BigDecimal>naturalOrder());
        return list.stream().collect(collector);
    }

    private static void serializeList(ArrayList<BigDecimal> list) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("store.txt");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            for (BigDecimal bd : list) {
                objectOutputStream.writeObject(bd);
            }
            objectOutputStream.writeObject(null);
            objectOutputStream.flush();
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            System.out.println("IOException");
        }
    }

    private static ArrayList<BigDecimal> deserializeList() {
        ArrayList<BigDecimal> list = new ArrayList<BigDecimal>();
        BigDecimal bd = null;
        try {
            FileInputStream fileInputStream = new FileInputStream("store.txt");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            Object obj = objectInputStream.readObject();
            while (obj != null) {
                list.add((BigDecimal) obj);
                obj = objectInputStream.readObject();
            }
            objectInputStream.close();
            fileInputStream.close();
        } catch (ClassNotFoundException e) {
            System.out.println("Class not found");
            return null;
        } catch (IOException e) {
            System.out.println("IOException");
            return null;
        }
        return list;
    }

    public static void main( String[] args ) throws IOException {
        ArrayList<BigDecimal> list = generateList(LIST_SIZE);

        System.out.println(add(list));
        System.out.println(average(list));
        System.out.println(topPercent(TOP_PERCENT, list));

        serializeList(list);
        list = deserializeList();
        System.out.println(list);
    }
}
