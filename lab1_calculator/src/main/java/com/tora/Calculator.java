package com.tora;

import java.util.Objects;

public class Calculator {

    private int last_result;

    public Calculator() {
        this.last_result = 0;
    }

    public int op_addition(int x, int y) {
        return x + y;
    }

    public int op_subtraction(int x, int y) {
        return x - y;
    }

    public int op_multiplication(int x, int y) {
        return x * y;
    }

    public int op_division(int x, int y) {
        if(y == 0)
            throw new RuntimeException("cannot divide by 0!");
        return x / y;
    }

    public int op_min(int x, int y) {
        return Math.min(x, y);
    }

    public int op_max(int x, int y) {
        return Math.max(x, y);
    }

    public int op_sqrt(int x) {
        if(x < 0)
            throw new RuntimeException("cannot SQRT negative integers");
        return (int) Math.sqrt(x);
    }

    public int op_last() {
        return last_result;
    }

    public int parse_command(String cmd) {
        String[] splitted = cmd.split(" ");

        if (Objects.equals(splitted[0], "last")) {
            return op_last();
        }

        if (splitted.length == 2 && Objects.equals(splitted[0], "sqrt")) {
            int op = Integer.parseInt(splitted[1]);
            last_result = op_sqrt(op);
            return last_result;
        }

        if(splitted.length != 3)
            throw new RuntimeException("invalid command!");

        int op1, op2;

        op1 = Integer.parseInt(splitted[0]);
        op2 = Integer.parseInt(splitted[2]);

        switch (splitted[1]) {
            case "+":
                last_result = op_addition(op1, op2);
                break;
            case "-":
                last_result = op_subtraction(op1, op2);
                break;
            case "/":
                last_result = op_division(op1, op2);
                break;
            case "*":
                last_result = op_multiplication(op1, op2);
                break;
            case "min":
                last_result = op_min(op1, op2);
                break;
            case "max":
                last_result = op_max(op1, op2);
                break;
            default:
                throw new RuntimeException("invalid operator!");
        }

        return last_result;
    }

}
