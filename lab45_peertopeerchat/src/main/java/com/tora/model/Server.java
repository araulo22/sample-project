package com.tora.model;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashSet;
import java.util.Set;

public class Server extends Thread {
    private ServerSocket serverSocket;
    private Set<ServerThread> serverThreadSet = new HashSet<ServerThread>();
    private String username;

    public Server(String port, String username) throws IOException {
        serverSocket = new ServerSocket(Integer.parseInt(port));
        this.username = username;
    }
    public void run() {
        try {
            while (true) {
                ServerThread serverThread = new ServerThread(serverSocket.accept(), this);
                serverThreadSet.add(serverThread);
                serverThread.start();
            }
        } catch (Exception e) {
            System.out.println("Server error.");
        }
    }
    void sendMessage(String message) {
        try {
            serverThreadSet.forEach(serverThread -> serverThread.getPrintWriter().println(message));
        } catch (Exception e) {
            System.out.println("Error sending the message.");
        }
    }
    public Set<ServerThread> getServerThreadSet() {
        return serverThreadSet;
    }

    public String getUsername() {
        return username;
    }
}
