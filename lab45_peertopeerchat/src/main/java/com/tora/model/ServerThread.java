package com.tora.model;

import javax.json.Json;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.Socket;

public class ServerThread extends Thread {
    private Server server;
    private Socket socket;
    private PrintWriter printWriter;
    public ServerThread(Socket socket, Server server) {
        this.server = server;
        this.socket = socket;
    }
    public void run() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            this.printWriter = new PrintWriter(socket.getOutputStream(), true);

            StringWriter stringWriter = new StringWriter();
            Json.createWriter(stringWriter).writeObject(Json.createObjectBuilder()
                    .add("username", server.getUsername())
                    .add("message", "!ack")
                    .build());
            server.sendMessage(stringWriter.toString());

            while (true) {
                server.sendMessage(bufferedReader.readLine());
            }
        } catch (Exception e) {
            System.out.println("Server thread error.");
            server.getServerThreadSet().remove(this);
        }
    }
    public PrintWriter getPrintWriter() {
        return printWriter;
    }
}
